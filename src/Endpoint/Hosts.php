<?php

declare(strict_types=1);

namespace Api\Client\Endpoint;

use Api\Client\HttpClient\Message\ResponseMediator;
use Api\Client\Sdk;

final class Hosts
{
    private Sdk $sdk;

    public function __construct(Sdk $sdk)
    {
        $this->sdk = $sdk;
    }

    public function getAll(): array
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->get('/hosts'));
    }
    public function getOne($id): array
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->get('/hosts/' . $id));
    }
    public function counts(): array
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->get('/hosts/count'));
    }

    public function create($data)
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->post('/hosts', [], json_encode($data)));
    }

    public function update($id, $data)
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->put('/hosts/' . $id, [], json_encode($data)));
    }
    public function deleteOne($id)
    {
        return ResponseMediator::getContent($this->sdk->getHttpClient()->delete('/hosts/' . $id));
    }
}
