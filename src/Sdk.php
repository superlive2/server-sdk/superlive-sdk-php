<?php

declare(strict_types=1);

namespace Api\Client;

use Api\Client\Endpoint\Hosts;
use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;

final class Sdk
{
    private ClientBuilder $clientBuilder;

    private Options $options;

    public function __construct(Options $options = null)
    {
        $this->options = $options ?? new Options();

        $this->clientBuilder = $this->options->getClientBuilder();
        $uriFactory = $options->getUriFactory();
        $this->clientBuilder->addPlugin(
            new BaseUriPlugin($uriFactory->createUri('http://merchant.super-live.tv/'))
        );
        $this->clientBuilder->addPlugin(
            new HeaderDefaultsPlugin(
                [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            )
        );
    }

    public function hosts(): Hosts
    {
        return new Endpoint\Hosts($this);
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->clientBuilder->getHttpClient();
    }
}
